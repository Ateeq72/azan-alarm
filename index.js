var express = require('express');
var app = express();
var server = require('http').createServer(app);
var mysql = require('mysql');

var connection = mysql.createConnection(
    {
      host     : 'localhost',
      user     : 'root',
      password : '',
      database : 'azan_alarm',
    }
);

var port = 8888;

server.listen(port);
console.log('Started at port : ' + port);

app.get('/',function (req, res) {

    var today = new Date();

    var month = today.getMonth() + 1;
    month = month > 9 ? 
            month :
            '0' + month;
    
    var todayStr = today.getFullYear() + '-' + month + '-' + today.getDate();

    var queryString = 'SELECT * FROM timings where date = "'+ todayStr + '"';

    connection.query(queryString, function(err, rows, fields) {
        if (err) throw err;    

        console.log('Prayer Times: ', rows);
        res.send(rows);        
    });

});

app.get('/callback/:data',function (req, res) {

    res.send('received : ' + req.params.data);
    console.log( req.params.data);
});
