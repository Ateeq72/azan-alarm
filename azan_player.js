var child = require('child_process');
var mysql = require('mysql');

var today = new Date();

var month = today.getMonth() + 1;
month = month > 9 ? 
        month :
        '0' + month;

var todayStr = today.getFullYear() + '-' + month + '-' + today.getDate();

var queryString = 'SELECT * FROM timings where date = "'+ todayStr + '"';

var playNow = false;

var connection = mysql.createConnection(
    {
      host     : 'localhost',
      user     : 'root',
      password : '',
      database : 'azan_alarm',
    }
);

connection.query(queryString, function(err, rows, fields) {

    if (err) {
    
        throw err; 
    }

    var now = new Date();

    var minutes = now.getMinutes() > 9 ? 
        now.getMinutes() :
        '0' + now.getMinutes();

   var hrs = now.getHours() > 9 ?
	now.getHours() :                                            '0' + now.getHours();

    var nowTxt = hrs + ':' + minutes + ':00';

    var prayers = [
        'Fajr',
        'Dhuhr',
        'Asr',
        'Maghrib',
        'Isha',
    ];

    for (var i = 0; i < rows.length; i++) {

        if (prayers.indexOf(rows[i]['namaz']) == -1) continue;

        if (playNow) break;

        playNow = rows[i]['time'] == nowTxt;
        console.log(rows[i]['namaz'] + ' prayer time: ' + rows[i]['time']);
    }


    if (playNow) {

        playAzanNow();
    }
    else {

         console.log('skipping at : ' + now.toLocaleTimeString() + ' & txt: ' + nowTxt);
    }

    process.exit(0);

});


function playAzanNow() {

    var now = new Date();

    console.log('Playing Azan at ' + now.toLocaleTimeString());

    var azanFilePath = process.cwd() + '/assets/Adhan_Makkah.mp3';

    console.log('Playing Azan');
    child.exec("cvlc --play-and-exit --volume-step 256 --gain 2 " + azanFilePath);
}


