var mysql = require('mysql');
var request = require('request');

// 
// CREATE TABLE `azan_alarm`.`timings` 
// ( `id` INT NOT NULL AUTO_INCREMENT , `date` DATE NOT NULL , `namaz` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL , `time` TIME NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;
// ALTER TABLE `timings` ADD `hijri_date` DATE NOT NULL AFTER `time`;
// ALTER TABLE `timings` ADD `hijri_month` VARCHAR(255) NOT NULL AFTER `hijri_date`;
// create schema azan_alarm CHARACTER SET utf8 COLLATE utf8_unicode_ci;

// API ref: https://aladhan.com/prayer-times-api#GetCalendar

var url = "http://api.aladhan.com/v1/calendar";

var connection = mysql.createConnection(
    {
      host     : 'localhost',
      user     : 'root',
      password : '',
      database : 'azan_alarm',
    }
);

var options = {
    method: 'GET',
    url: url,
    qs: {
      longitude: '80.27072143554688',
      latitude: '13.082679748535156',
      method: '1',
      school: '1',
    },
    headers: {
      useQueryString: true
    }
  };
  
  request(options, function (error, response, body) {
      
    if (error) throw new Error(error);

    console.log(body);

    var responseJSON = JSON.parse(body);

    var truncateQuery = 'truncate timings;';

    var insertQuery = "insert into timings (date, namaz, time, hijri_date, hijri_month) values ";

    var queryData = [];

    for (var i = 0; i < responseJSON['data'].length; i++) {

      var data = responseJSON['data'][i];

      var month =  data['date']['gregorian']['month']['number'] > 9 ? 
                    data['date']['gregorian']['month']['number'] :
                    '0' + data['date']['gregorian']['month']['number'];

      var date = data['date']['gregorian']['year'] + '-' + month + '-' + data['date']['gregorian']['day'];

      var hijriMonth = data['date']['hijri']['month']['number'] > 9 ? 
                          data['date']['hijri']['month']['number'] :
                          '0' + data['date']['hijri']['month']['number'];

      var hijri_date = data['date']['hijri']['year'] + '-' + hijriMonth + '-' + data['date']['hijri']['day'];

      var hijriMonthName = data['date']['hijri']['month']['en'];

      var timings = Object.entries(data['timings']);

      for (var j = 0; j < timings.length; j++) {

        queryData.push([
            date,
            timings[j][0],
            timings[j][1].split(' ')[0],
            hijri_date,
            hijriMonthName
        ]);

      }
      
    }

    insertQuery += JSON.stringify(queryData).replace('[', '').split('[').join('(').split(']').join(')');

    insertQuery = insertQuery.substr(0, insertQuery.lastIndexOf(')'));

    connection.query(truncateQuery, function(err, rows, fields) {

      if (err) throw err;    
      console.log('Truncated Successfully: ', rows);   

    });

    connection.query(insertQuery, function(err, rows, fields) {

      if (err) throw err;    
      console.log('Inserted Successfully: ', rows);    

      process.exit(0);

    });

  });